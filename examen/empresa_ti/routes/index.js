var express = require('express');
var app = express.Router();

app.use(express.static('public'))
app.use(express.static('files'))

app.use('/static',express.static('public'))
app.get('/', function(req, res, next) {
  res.render('index', { title: 'Empresa de TI',message1 :'Galeria de sistemas' });
});

app.get('/galeria',function(req,res){
  res.render('galeria',{message:'Productos actualmente en stock'})
})

app.post('/galeria',function(req,res){
  res.end('Enviando nueva imagen')
})

app.get('/solicitud',function(req,res){
  res.end("Su presupuesto actualmente es de ... ")
})

app.get('/formulario',function(req,res){
  res.render('form',{message:'Rellene este formulario para solicitar el presupuesto',buttonMessage:'Enviar formulario'});
})
app.post('/formulario',function(req,res){
  res.end('Enviando formulario')
})

app.put('/',function(req,res){
  res.end('Recibimos un PUT en /user')
})
app.delete('/',function(req,res){
  res.end('Recibimos un delete en /user')
})
app.use(function(req,res,next){
  res.status(404).send('La pagina que esta solicitando no existe')
})

app.use(function(err,req,res,next){
  console.error(err.stack)
  res.status(500).send('Algo salio mal')
})

module.exports = app;

