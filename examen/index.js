var express = require('express')
var app = express()

app.get('/',function(req,res){
    res.end('Bienvenido a la pagina de inicio de la EMPRESA TI')
})

app.get('/galeria',function(req,res){
    res.end("Estas en galerias de Sistemas")
})

app.get('/solicitud',function(req,res){
    res.end("Solicitud de presupuesto")
})

app.post('/',function(req,res){
    res.end('Llamada POST misma Url')
})

app.put('/',function(req,res){
    res.end('Recibimos un PUT en /user')
})
app.delete('/',function(req,res){
    res.end('Recibimos un delete en /user')
})
app.use(function(req,res,next){
    res.status(404).send('La pagina que esta solicitando no existe')
})

app.use(function(err,req,res,next){
    console.error(err.stack)
    res.status(500).send('Algo salio mal')
})

app.listen(3000,function(){
    console.log('Corriendo');
})